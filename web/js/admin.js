$(document).ready(function() {
  $("button.linked").click(function(e) {
    e.preventDefault();
    window.location.href = $(this).attr('href');
  });
});