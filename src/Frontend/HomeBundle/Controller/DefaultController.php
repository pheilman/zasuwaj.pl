<?php

namespace Frontend\HomeBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

class DefaultController extends Controller
{
    public function indexAction()
    {
        $table = $this->getDoctrine()->getRepository('AdminPanelBundle:Homepage');
        $page = $table->findAll();

        $page_data = array();

        foreach ($page as $value) {
          $page_data[$value->getKeyValue()]['title'] = $value->getTitleValue();
          $page_data[$value->getKeyValue()]['text'] = $value->getTextValue();
        }

        $table = $this->getDoctrine()->getRepository('AdminPanelBundle:Settings');
        $page = $table->findAll();

        $settings = array();

        foreach ($page as $value) {
          $settings[$value->getKeyValue()]['data'] = $value->getTextValue();
        }

        $table = $this->getDoctrine()->getRepository('AdminPanelBundle:Description');
        $meta = $table->getHomepage();

        $table = $this->getDoctrine()->getRepository('AdminPanelBundle:Keywords');
        $footerVal = $table->createQueryBuilder('k')
          ->where('k.visible = :visible')
          ->setParameter('visible', true)
          ->orderBy('k.orderValue')
          ->getQuery()
          ->getResult();

        $footer = '';
        foreach ($footerVal as $val) {
          $footer .= ' | '.$val->getTextValue();
        }

        return $this->render('FrontendHomeBundle:Default:index.html.php', array('page_data' => $page_data, 'settings' => $settings, 'meta' => $meta, 'footer' => $footer));
    }

    public function aboutAction()
    {
        $table = $this->getDoctrine()->getRepository('AdminPanelBundle:Description');
        $meta = $table->getAbout();

        $table = $this->getDoctrine()->getRepository('AdminPanelBundle:Keywords');
        $footerVal = $table->createQueryBuilder('k')
          ->where('k.visible = :visible')
          ->setParameter('visible', true)
          ->orderBy('k.orderValue')
          ->getQuery()
          ->getResult();

        $footer = '';
        foreach ($footerVal as $val) {
          $footer .= ' | '.$val->getTextValue();
        }
        
        return $this->render('FrontendHomeBundle:Default:about.html.php', array('meta' => $meta, 'footer' => $footer));
    }

    public function trainersAction()
    {
        $table = $this->getDoctrine()->getRepository('AdminPanelBundle:Description');
        $meta = $table->getTrainers();

        $table = $this->getDoctrine()->getRepository('AdminPanelBundle:Trainer');
        $page = $table->createQueryBuilder('t')
          ->where('t.visible = :visible')
          ->setParameter('visible', true)
          ->orderBy('t.peopleOrder')
          ->getQuery()
          ->getResult();

        $table = $this->getDoctrine()->getRepository('AdminPanelBundle:About');
        $page2 = $table->findAll();

        $page_data = array();

        foreach ($page2 as $value) {
          $page_data[$value->getKeyValue()]['title'] = $value->getTitleValue();
          $page_data[$value->getKeyValue()]['text'] = $value->getTextValue();
        }


        $table = $this->getDoctrine()->getRepository('AdminPanelBundle:Keywords');
        $footerVal = $table->createQueryBuilder('k')
          ->where('k.visible = :visible')
          ->setParameter('visible', true)
          ->orderBy('k.orderValue')
          ->getQuery()
          ->getResult();

        $footer = '';
        foreach ($footerVal as $val) {
          $footer .= ' | '.$val->getTextValue();
        }


        return $this->render('FrontendHomeBundle:Default:trainers.html.php', array('trainers' => $page, 'about' => $page_data['text1'], 'meta' => $meta, 'footer' => $footer));
    }

    public function faqAction()
    {
        $table = $this->getDoctrine()->getRepository('AdminPanelBundle:FAQ');
        $page = $table->createQueryBuilder('faq')
          ->where('faq.visible = :visible')
          ->setParameter('visible', true)
          ->orderBy('faq.faqOrder')
          ->getQuery()
          ->getResult();

        $table = $this->getDoctrine()->getRepository('AdminPanelBundle:Description');
        $meta = $table->getFAQ();

        $table = $this->getDoctrine()->getRepository('AdminPanelBundle:Keywords');
        $footerVal = $table->createQueryBuilder('k')
          ->where('k.visible = :visible')
          ->setParameter('visible', true)
          ->orderBy('k.orderValue')
          ->getQuery()
          ->getResult();

        $footer = '';
        foreach ($footerVal as $val) {
          $footer .= ' | '.$val->getTextValue();
        }

        return $this->render('FrontendHomeBundle:Default:faq.html.php', array('faq' => $page, 'meta' => $meta, 'footer' => $footer));
    }

    public function shaprAction()
    {
        $table = $this->getDoctrine()->getRepository('AdminPanelBundle:ShAPr');
        $page = $table->findAll();

        $page_data = array();

        foreach ($page as $value) {
          $page_data[$value->getKeyValue()]['title'] = $value->getTitleValue();
          $page_data[$value->getKeyValue()]['text'] = $value->getTextValue();
        }

        $table = $this->getDoctrine()->getRepository('AdminPanelBundle:Description');
        $meta = $table->getShAPr();

        $table = $this->getDoctrine()->getRepository('AdminPanelBundle:Keywords');
        $footerVal = $table->createQueryBuilder('k')
          ->where('k.visible = :visible')
          ->setParameter('visible', true)
          ->orderBy('k.orderValue')
          ->getQuery()
          ->getResult();

        $footer = '';
        foreach ($footerVal as $val) {
          $footer .= ' | '.$val->getTextValue();
        }

        return $this->render('FrontendHomeBundle:Default:shapr.html.php', array('page_data' => $page_data, 'meta' => $meta, 'footer' => $footer));
    }

    public function contactAction(Request $request, $sent)
    {
        $form = $this->createFormBuilder()
          ->add('name', 'text', array('label' => 'Imię i nazwisko'))
          ->add('email', 'email', array('label' => 'E-mail'))
          ->add('subject', 'text', array('label' => 'Temat'))
          ->add('message', 'textarea', array('label' => 'Wiadomość'))
          ->getForm();

        if ($request->getMethod() == 'POST') {
          $form->bindRequest($request);

          if ($form->isValid()) {
            $formData = $form->getData();
            $message = \Swift_Message::newInstance()
                ->setSubject($formData['subject'])
                ->setFrom(array($formData['email'] => $formData['name']))
                ->setTo('kontakt@zasuwaj.pl')
                ->setBody($formData['message'])
              ;
            $this->get('mailer')->send($message);

            return $this->redirect($this->generateUrl('frontend_home_contactpage_sent'));
          }
        }

        $table = $this->getDoctrine()->getRepository('AdminPanelBundle:Settings');
        $page = $table->findAll();

        $settings = array();

        foreach ($page as $value) {
          $settings[$value->getKeyValue()]['data'] = $value->getTextValue();
        }

        $table = $this->getDoctrine()->getRepository('AdminPanelBundle:Description');
        $meta = $table->getContact();

        $table = $this->getDoctrine()->getRepository('AdminPanelBundle:Keywords');
        $footerVal = $table->createQueryBuilder('k')
          ->where('k.visible = :visible')
          ->setParameter('visible', true)
          ->orderBy('k.orderValue')
          ->getQuery()
          ->getResult();

        $footer = '';
        foreach ($footerVal as $val) {
          $footer .= ' | '.$val->getTextValue();
        }

        return $this->render('FrontendHomeBundle:Default:contact.html.php', array(
          'form' => $form->createView(),
          'settings' => $settings,
          'meta' => $meta,
          'footer' => $footer,
          'sent' => $sent,
        ));
    }

    public function cookiesAction()
    {
        $table = $this->getDoctrine()->getRepository('AdminPanelBundle:Keywords');
        $footerVal = $table->createQueryBuilder('k')
          ->where('k.visible = :visible')
          ->setParameter('visible', true)
          ->orderBy('k.orderValue')
          ->getQuery()
          ->getResult();

        $footer = '';
        foreach ($footerVal as $val) {
          $footer .= ' | '.$val->getTextValue();
        }

        return $this->render('FrontendHomeBundle:Default:cookies.html.php', array('footer' => $footer));
    }
}
