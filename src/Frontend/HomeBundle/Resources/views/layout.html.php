<?php $view->extend('::base.html.php'); ?>

<?php echo $view->render('FrontendHomeBundle::header.html.php'); ?>

<div class="content">
  <?php $view['slots']->output('_content') ?>
</div>

<?php echo $view->render('FrontendHomeBundle::footer.html.php'); ?>