<div class="header">
  <div class="banner">
    <div class="banner-left">
      <?php $view['slots']->output('header-title', '<p class="title">Zasuwaj</p><p class="subtitle">Szkoła jazdy na rolkach</p>'); ?>
    </div>
    <div class="banner-right">
      <p>Umów się na pierwsze zajęcia</p>
      <a class="button reserve-button" href="<?php echo $view['router']->generate('frontend_home_contactpage'); ?>">Rezerwuj termin</a>
    </div>
  </div>
  <div class="menu-line">
    <div class="menu">
      <a class="menu-item <?php echo ($app->getRequest()->getPathInfo() == $view['router']->generate('frontend_home_homepage')) ? 'menu-item-active' : ''; ?>" id="menu-item-1" href="<?php echo $view['router']->generate('frontend_home_homepage'); ?>"><h2>Strona główna</h2></a>
      <a class="menu-item <?php echo ($app->getRequest()->getPathInfo() == $view['router']->generate('frontend_home_aboutpage')) ? 'menu-item-active' : ''; ?>" id="menu-item-2" href="<?php echo $view['router']->generate('frontend_home_aboutpage'); ?>"><h2>Aktualności</h2></a>
      <a class="menu-item <?php echo ($app->getRequest()->getPathInfo() == $view['router']->generate('frontend_home_trainerspage')) ? 'menu-item-active' : ''; ?>" id="menu-item-3" href="<?php echo $view['router']->generate('frontend_home_trainerspage'); ?>"><h2>Instruktorzy</h2></a>
      <a class="menu-item <?php echo ($app->getRequest()->getPathInfo() == $view['router']->generate('frontend_home_shaprpage')) ? 'menu-item-active' : ''; ?>" id="menu-item-4" href="<?php echo $view['router']->generate('frontend_home_shaprpage'); ?>"><h2>Oferta</h2></a>
      <a class="menu-item <?php echo ($app->getRequest()->getPathInfo() == $view['router']->generate('frontend_home_faqpage')) ? 'menu-item-active' : ''; ?>" id="menu-item-5" href="<?php echo $view['router']->generate('frontend_home_faqpage'); ?>"><h2>Pytania i Odpowiedzi</h2></a>
      <a class="menu-item <?php echo ($app->getRequest()->getPathInfo() == $view['router']->generate('frontend_home_contactpage')) ? 'menu-item-active' : ''; ?>" id="menu-item-6" href="<?php echo $view['router']->generate('frontend_home_contactpage'); ?>"><h2>Kontakt</h2></a>
    </div>
  </div>
</div>