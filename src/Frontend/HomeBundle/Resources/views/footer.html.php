<div class="footer">
  <div class="keywords">
    <div class="inside">
      <p>
        <a href="<?php echo $view['router']->generate('frontend_home_cookies'); ?>">Informacje o plikach Cookies</a>
        <?php $view['slots']->output('footer-line', ''); ?>
      </p>
    </div>
  </div>
  <div class="copyright">
    <p>
      &copy; 2013 by Zasuwaj. All rights reserved.
    </p>
  </div>
</div>
<script type="text/javascript">

var _gaq = _gaq || [];
_gaq.push(['_setAccount', 'UA-40580262-1']);
_gaq.push(['_trackPageview']);

(function() {
var ga = document.createElement('script'); ga.type = 'text/javascript'; ga.async = true;
ga.src = ('https:' == document.location.protocol ? 'https://ssl' : 'http://www') + '.google-analytics.com/ga.js';
var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(ga, s);
})();

</script>
