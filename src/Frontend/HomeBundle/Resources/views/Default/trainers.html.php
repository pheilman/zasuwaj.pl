<?php $view->extend('FrontendHomeBundle::layout.html.php'); ?>
<?php $view['slots']->set('title', $meta->getTitle()); ?>
<?php $view['slots']->set('description', $meta->getDescription()); ?>
<?php $view['slots']->set('footer-line', $footer); ?>

<div class="one trainers">
  <h1 class="blue"><?php echo $about['title']; ?></h2>
  <div class="text">
    <?php echo $about['text']; ?>
  </div>
  <?php foreach ($trainers as $trainer) { ?>
    <div class="trainer">
      <div class="image">
        <img src="<?php echo $this['imagine']->filter('/'.$trainer->getWebPath().'/'.$trainer->getPhoto(), 'trainer_thumb'); ?>" />
      </div>
      <div class="text">
        <h3><?php echo $trainer->getName(); ?></h3>
        <p><?php echo $trainer->getAbout(); ?></p>
      </div>
    </div>
  <?php } ?>
</div>