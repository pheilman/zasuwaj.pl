<?php $view->extend('FrontendHomeBundle::layout.html.php'); ?>
<?php $view['slots']->set('title', $meta->getTitle()); ?>
<?php $view['slots']->set('description', $meta->getDescription()); ?>
<?php $view['slots']->set('footer-line', $footer); ?>

<?php if($sent) { ?>
  <div class="one mail-sent">
      <p>Mail został wysałny.</p>
  </div>
<?php } ?>
<div class="one contact">
  <div class="left">
    <img class="border" src="<?php echo $view['assets']->getUrl('images/contact-photo.jpg'); ?>" />
  </div>
  <div class="right">
    <h1 class="blue">Kontakt</h1>
    <h3>Masz pytania? Napisz!</h3>
    <div class="line">
      <div class="mail-number">
        <p><?php echo $settings['mail']['data']; ?><br />Tel: <?php echo substr($settings['phone']['data'], 0, 3); ?> <?php echo substr($settings['phone']['data'], 3, 3); ?> <?php echo substr($settings['phone']['data'], 6, 3); ?></p>
      </div>
      <div class="links">
        <?php /*
        <a href="http://<?php echo $settings['fb_link']['data']; ?>" target="_blank"><img src="<?php echo $view['assets']->getUrl('images/fb-link.png'); ?>" /></a>
        <a href="http://<?php echo $settings['yt_link']['data']; ?>" target="_blank"><img src="<?php echo $view['assets']->getUrl('images/yt-link.png'); ?>" /></a>
        */ ?>
      </div>
    </div>
    <form class="contact-form" action="<?php echo $view['router']->generate('frontend_home_contactpage'); ?>" method="post" <?php echo $view['form']->enctype($form); ?>>
      <?php echo $view['form']->label($form['name']); ?>
      <?php echo $view['form']->widget($form['name']); ?>
      <?php echo $view['form']->label($form['email']); ?>
      <?php echo $view['form']->widget($form['email']); ?>
      <?php echo $view['form']->label($form['subject']); ?>
      <?php echo $view['form']->widget($form['subject']); ?>
      <?php echo $view['form']->label($form['message']); ?>
      <?php echo $view['form']->widget($form['message']); ?>
      <?php echo $view['form']->widget($form); ?>
      <input class="button send-button" value="Wyślij" type="submit" />
    </form>
  </div>
</div>