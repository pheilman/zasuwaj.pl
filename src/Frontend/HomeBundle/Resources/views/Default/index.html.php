<?php $view->extend('FrontendHomeBundle::layout.html.php'); ?>
<?php $view['slots']->set('title', $meta->getTitle()); ?>
<?php $view['slots']->set('description', $meta->getDescription()); ?>
<?php $view['slots']->set('footer-line', $footer); ?>
<?php $view['slots']->set('header-title', '<h1>Zasuwaj<br /><span class="subtitle">Szkoła jazdy na rolkach</span></h1>'); ?>


<div class="one border-top-bottom">
  <div class="home-info">
    <?php /*
    <div class="links-line">
      <a href="http://<?php echo $settings['fb_link']['data']; ?>" target="_blank"><img src="<?php echo $view['assets']->getUrl('images/fb-link.png'); ?>" /></a>
      <a href="http://<?php echo $settings['yt_link']['data']; ?>" target="_blank"><img src="<?php echo $view['assets']->getUrl('images/yt-link.png'); ?>" /></a>
    </div>
    */ ?>
    <?php /*                       usuń to poniżej, jak będziesz dodawał linki spowrotem */ ?>
    <div class="about-info" style="margin-top: 55px;">
      <h3><?php echo $page_data['text1']['title']; ?></h3>
      <div class="text">
        <?php echo $page_data['text1']['text']; ?>
      </div>
      <div class="more">
        <a class="button more-button" href="<?php echo $view['router']->generate('frontend_home_trainerspage'); ?>">wiecej</a>
      </div>
    </div>
  </div>
</div>
<div class="one more-info">
  <div class="one-third">
    <h3 class="wheel"><?php echo $page_data['column1']['title']; ?></h3>
    <div class="text">
      <?php echo $page_data['column1']['text']; ?>
    </div>
  </div>
  <div class="one-third">
    <a href="<?php echo $view['router']->generate('frontend_home_shaprpage'); ?>#indywidualne"><h3 class="wheel"><?php echo $page_data['column2']['title']; ?></h3></a>
    <div class="text">
      <?php echo $page_data['column2']['text']; ?>
    </div>
  </div>
  <div class="one-third">
    <a href="<?php echo $view['router']->generate('frontend_home_shaprpage'); ?>#slalomowe"><h3 class="wheel"><?php echo $page_data['column3']['title']; ?></h3></a>
    <div class="text">
      <?php echo $page_data['column3']['text']; ?>
    </div>
  </div>
</div>
<div class="one contact-info">
  <div class="two-thirds" style="overflow: hidden;">
    <?php /*
    <h3><?php echo $page_data['text2']['title']; ?></h3>
    <div class="text">
      <?php echo $page_data['text2']['text']; ?>
    </div>
    */ ?>
  </div>
  <div class="one-third" style="margin-left: 684px;">
    <h3>Kontakt</h3>
    <div class="text">
      <?php echo $settings['mail']['data']; ?><br />
      Tel: <?php echo substr($settings['phone']['data'], 0, 3); ?> <?php echo substr($settings['phone']['data'], 3, 3); ?> <?php echo substr($settings['phone']['data'], 6, 3); ?>
    </div>
  </div>
</div>
