<?php $view->extend('FrontendHomeBundle::layout.html.php'); ?>
<?php $view['slots']->set('title', $meta->getTitle()); ?>
<?php $view['slots']->set('description', $meta->getDescription()); ?>
<?php $view['slots']->set('footer-line', $footer); ?>

<div class="one faq">
  <h1 class="blue">Pytania i odpowiedzi</h1>
  <?php foreach ($faq as $val) { ?>
    <h3 class="blue"><?php echo $val->getQuestion(); ?></h3>
    <div class="answer"><?php echo $val->getAnswer(); ?></div>
  <?php } ?>
</div>