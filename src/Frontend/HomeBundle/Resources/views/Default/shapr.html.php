<?php $view->extend('FrontendHomeBundle::layout.html.php'); ?>
<?php $view['slots']->set('title', $meta->getTitle()); ?>
<?php $view['slots']->set('description', $meta->getDescription()); ?>
<?php $view['slots']->set('footer-line', $footer); ?>

<div class="one shapr">
  <h1 class="blue">Oferta</h1>
</div>
<div class="one more-info shapr2" id="indywidualne">
  <h3 class="blue">
    <?php echo $page_data['text1']['title']; ?>
  </h3>
  <div class="text">
    <?php echo $page_data['text1']['text']; ?>
  </div>
</div>
<div class="one more-info">
  <div class="one-third">
    <h3 class="wheel1">
      <?php echo $page_data['text2']['title']; ?>
    </h3>
    <div class="text">
      <?php echo $page_data['text2']['text']; ?>
    </div>
  </div>
  <div class="one-third">
    <h3 class="wheel2">
      <?php echo $page_data['text3']['title']; ?>
    </h3>
    <div class="text">
      <?php echo $page_data['text3']['text']; ?>
    </div>
  </div>
  <div class="one-third">
    <h3 class="wheel3">
      <?php echo $page_data['text4']['title']; ?>
    </h3>
    <div class="text">
      <?php echo $page_data['text4']['text']; ?>
    </div>
  </div>
</div>
<div class="one more-info shapr2" id="slalomowe">
  <h3 class="blue">
    <?php echo $page_data['text5']['title']; ?>
  </h3>
  <div class="text">
    <?php echo $page_data['text5']['text']; ?>
  </div>
</div>