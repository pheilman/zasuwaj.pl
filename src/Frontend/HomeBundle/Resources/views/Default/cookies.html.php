<?php $view->extend('FrontendHomeBundle::layout.html.php'); ?>
<?php $view['slots']->set('title', 'Zasuwaj.pl - Pliki cookies.'); ?>
<?php $view['slots']->set('description', 'Pliki cookies.'); ?>
<?php $view['slots']->set('robot', '<meta name="robots" content="noindex,nofollow" />'); ?>
<?php $view['slots']->set('footer-line', $footer); ?>

<div class="one cookies">
  <h3 class="blue">Pliki Cookie:</h3>
  <ol>
    <li>Podczas korzystania przez Ciebie ze strony Zasuwaj.pl na Twoim urządzeniu końcowym zapisywane są niewielkie pliki tekstowe, zawierające informacje niezbędne do prawidłowego korzystania z serwisu, tzw. pliki cookie.</li>
    <li>W trakcie użytkowania przez Ciebie ze strony Zasuwaj.pl korzystamy z dwóch rodzajów plików cookie: sesyjnych oraz stałych. Pliki sesyjne to pliki tymczasowe, które przechowywane są w Twoim urządzeniu do czasu zakończenia sesji danej przeglądarki. Stałe pliki są przechowywane w Twoim urządzeniu przez czas określony w parametrach tych plików lub do momentu ich usunięcia przez Ciebie.</li>
    <li>Pliki te są wykorzystywane przez nas:
      <ul>
        <li>W celach statystycznych. Serwis Zasuwaj.pl korzysta z narzędzi google służących do badania zachowań użytkowników witryny.</li>
        <li>W Celach społecznościowych. Pliki cookie umożliwiają integrację serwisów społecznościowych ze sklepem</li>
      </ul>
    </li>
    <li>Zawartość plików cookies nie pozwala na identyfikację użytkowników, za ich pomocą nie są przetwarzane i przechowywane dane osobowe.</li>
    <li>Jeżeli uważasz, że obecność plików cookie narusza Twoją prywatność, możesz je w każdej chwili wyłączyć dla naszej witryny albo dla wszystkich witryn przeglądanych za pomocą danej przeglądarki:
      <ul>
        <li><a href="http://support.mozilla.org/pl/kb/ciasteczka#w_ustawienia-ciasteczek">Blokada przyjmowania plików "cookie" w Firefox</a></li>
        <li><a href="http://support.microsoft.com/kb/278835/pl">Blokada przyjmowania plików "cookie" w Interent Explorer</a></li>
        <li><a href="http://support.google.com/chrome/bin/answer.py?hl=pl&answer=95647">Blokada przyjmowania plików "cookie" w Chrome</a></li>
        <li><a href="http://support.apple.com/kb/PH5042">Blokada przyjmowania plików "cookie" w Safari</a></li>
        <li>Każdy model telefonu i tablet może tę funkcję obsługiwać w inny sposób. Dlatego w przypadku przeglądania strony na urządzeniach mobilnych zachęcamy do zapoznania się z opcjami prywatności w dokumentacji na stronie internetowej producenta Twojego urządzenia.</li>
      </ul>
    </li>
  </ol>
</div>