<?php

namespace Admin\PanelBundle\Form;

use Symfony\Component\Form\AbstractType;
use Symfony\Component\Form\FormBuilderInterface;
use Symfony\Component\OptionsResolver\OptionsResolverInterface;

class HomepageType extends AbstractType
{
    public function buildForm(FormBuilderInterface $builder, array $options)
    {
        $builder
            ->add('keyValue')
            ->add('titleValue')
            ->add('textValue')
        ;
    }

    public function setDefaultOptions(OptionsResolverInterface $resolver)
    {
        $resolver->setDefaults(array(
            'data_class' => 'Admin\PanelBundle\Entity\Homepage'
        ));
    }

    public function getName()
    {
        return 'admin_panelbundle_homepagetype';
    }
}
