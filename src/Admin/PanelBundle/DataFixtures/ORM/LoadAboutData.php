<?php
namespace Admin\PanelBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Admin\PanelBundle\Entity\About;

class LoadAboutData implements FixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $text1 = new About();
        $text1->setKeyValue('text1');
        $text1->setTitleValue('Instruktorzy');
        $text1->setTextValue('tekst');

        $manager->persist($text1);
        $manager->flush();

        /*
        $text1 = new About();
        $text1->setKeyValue('text1');
        $text1->setTitleValue('nie wyświetlany');
        $text1->setTextValue('tekst');

        $manager->persist($text1);
        $manager->flush();

        $column1 = new About();
        $column1->setKeyValue('column1');
        $column1->setTitleValue('Kolumna 1');
        $column1->setTextValue('tekst');

        $manager->persist($column1);
        $manager->flush();

        $column2 = new About();
        $column2->setKeyValue('column2');
        $column2->setTitleValue('Kolumna 2');
        $column2->setTextValue('tekst');

        $manager->persist($column2);
        $manager->flush();

        $column3 = new About();
        $column3->setKeyValue('column3');
        $column3->setTitleValue('Kolumna 3');
        $column3->setTextValue('tekst');

        $manager->persist($column3);
        $manager->flush();
        */
    }
}
?>