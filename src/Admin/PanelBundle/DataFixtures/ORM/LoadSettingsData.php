<?php
namespace Admin\PanelBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Admin\PanelBundle\Entity\Settings;

class LoadSettingsData implements FixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $mail = new Settings();
        $mail->setKeyValue('mail');
        $mail->setTextValue('mail@mail.com');

        $manager->persist($mail);
        $manager->flush();

        $phone = new Settings();
        $phone->setKeyValue('phone');
        $phone->setTextValue('123456789');
        
        $manager->persist($phone);
        $manager->flush();

        $fb_link = new Settings();
        $fb_link->setKeyValue('fb_link');
        $fb_link->setTextValue('www.facebook.com');
        
        $manager->persist($fb_link);
        $manager->flush();

        $yt_link = new Settings();
        $yt_link->setKeyValue('yt_link');
        $yt_link->setTextValue('www.youtube.com');
        
        $manager->persist($yt_link);
        $manager->flush();
    }
}
?>