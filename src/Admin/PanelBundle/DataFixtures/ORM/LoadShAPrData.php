<?php
namespace Admin\PanelBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Admin\PanelBundle\Entity\ShAPr;

class LoadShAPrData implements FixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $text1 = new ShAPr();
        $text1->setKeyValue('text1');
        $text1->setTitleValue('Początkujący');
        $text1->setTextValue('tekst');

        $manager->persist($text1);
        $manager->flush();

        $text1 = new ShAPr();
        $text1->setKeyValue('text2');
        $text1->setTitleValue('Średniozaawansowani');
        $text1->setTextValue('tekst');

        $manager->persist($text1);
        $manager->flush();
        
        $text1 = new ShAPr();
        $text1->setKeyValue('text3');
        $text1->setTitleValue('Zaawansowani');
        $text1->setTextValue('tekst');

        $manager->persist($text1);
        $manager->flush();
        
        $text1 = new ShAPr();
        $text1->setKeyValue('text4');
        $text1->setTitleValue('Bezpłatne warsztaty');
        $text1->setTextValue('tekst');

        $manager->persist($text1);
        $manager->flush();
    }
}