<?php
namespace Admin\PanelBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Admin\PanelBundle\Entity\Homepage;

class LoadHomepageData implements FixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $text1 = new Homepage();
        $text1->setKeyValue('text1');
        $text1->setTitleValue('O nas');
        $text1->setTextValue('tekst');

        $manager->persist($text1);
        $manager->flush();

        $text2 = new Homepage();
        $text2->setKeyValue('text2');
        $text2->setTitleValue('O nas');
        $text2->setTextValue('tekst');

        $manager->persist($text2);
        $manager->flush();

        $column1 = new Homepage();
        $column1->setKeyValue('column1');
        $column1->setTitleValue('Kolumna 1');
        $column1->setTextValue('tekst');

        $manager->persist($column1);
        $manager->flush();

        $column2 = new Homepage();
        $column2->setKeyValue('column2');
        $column2->setTitleValue('Kolumna 2');
        $column2->setTextValue('tekst');

        $manager->persist($column2);
        $manager->flush();

        $column3 = new Homepage();
        $column3->setKeyValue('column3');
        $column3->setTitleValue('Kolumna 3');
        $column3->setTextValue('tekst');

        $manager->persist($column3);
        $manager->flush();
    }
}
?>