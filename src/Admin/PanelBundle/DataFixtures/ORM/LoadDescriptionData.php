<?php
namespace Admin\PanelBundle\DataFixtures\ORM;

use Doctrine\Common\DataFixtures\FixtureInterface;
use Doctrine\Common\Persistence\ObjectManager;
use Admin\PanelBundle\Entity\Description;

class LoadDescriptionData implements FixtureInterface
{
    /**
     * {@inheritDoc}
     */
    public function load(ObjectManager $manager)
    {
        $homepage = new Description();
        $homepage->setPage('homepage');
        $homepage->setTitle('Strona główna');
        $homepage->setDescription('opis');

        $manager->persist($homepage);
        $manager->flush();

        $about = new Description();
        $about->setPage('about');
        $about->setTitle('Aktualności');
        $about->setDescription('opis');

        $manager->persist($about);
        $manager->flush();

        $contact = new Description();
        $contact->setPage('contact');
        $contact->setTitle('Kontakt');
        $contact->setDescription('opis');

        $manager->persist($contact);
        $manager->flush();

        $trainers = new Description();
        $trainers->setPage('trainers');
        $trainers->setTitle('Instruktorzy');
        $trainers->setDescription('opis');

        $manager->persist($trainers);
        $manager->flush();

        $faq = new Description();
        $faq->setPage('faq');
        $faq->setTitle('FAQ');
        $faq->setDescription('opis');

        $manager->persist($faq);
        $manager->flush();

        $faq = new Description();
        $faq->setPage('shapr');
        $faq->setTitle('Oferta');
        $faq->setDescription('opis');

        $manager->persist($faq);
        $manager->flush();
    }
}
?>