<?php

namespace Admin\PanelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Description
 */
class Description
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $page;

    /**
     * @var string
     */
    private $title;

    /**
     * @var string
     */
    private $description;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set page
     *
     * @param string $page
     * @return Description
     */
    public function setPage($page)
    {
        $this->page = $page;
    
        return $this;
    }

    /**
     * Get page
     *
     * @return string 
     */
    public function getPage()
    {
        return $this->page;
    }

    /**
     * Set title
     *
     * @param string $title
     * @return Description
     */
    public function setTitle($title)
    {
        $this->title = $title;
    
        return $this;
    }

    /**
     * Get title
     *
     * @return string 
     */
    public function getTitle()
    {
        return $this->title;
    }

    /**
     * Set description
     *
     * @param string $description
     * @return Description
     */
    public function setDescription($description)
    {
        $this->description = $description;
    
        return $this;
    }

    /**
     * Get description
     *
     * @return string 
     */
    public function getDescription()
    {
        return $this->description;
    }

    public function getPageName()
    {
        switch($this->page)
        {
            case 'homepage':
                return 'Strona główna';
            case 'about':
                return 'Aktualności';
            case 'contact':
                return 'Kontakt';
            case 'trainers':
                return 'Instruktorzy';
            case 'faq':
                return 'Pytania i odpowiedzi';
            case 'shapr':
                return 'Oferta';
            default:
                return $this->page;
        }
    }
}