<?php

namespace Admin\PanelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Keywords
 */
class Keywords
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $textValue;

    /**
     * @var integer
     */
    private $orderValue;

    /**
     * @var boolean
     */
    private $visible;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set textValue
     *
     * @param string $textValue
     * @return Keywords
     */
    public function setTextValue($textValue)
    {
        $this->textValue = $textValue;
    
        return $this;
    }

    /**
     * Get textValue
     *
     * @return string 
     */
    public function getTextValue()
    {
        return $this->textValue;
    }

    /**
     * Set orderValue
     *
     * @param integer $orderValue
     * @return Keywords
     */
    public function setOrderValue($orderValue)
    {
        $this->orderValue = $orderValue;
    
        return $this;
    }

    /**
     * Get orderValue
     *
     * @return integer 
     */
    public function getOrderValue()
    {
        return $this->orderValue;
    }

    /**
     * Set visible
     *
     * @param boolean $visible
     * @return Keywords
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;
    
        return $this;
    }

    /**
     * Get visible
     *
     * @return boolean 
     */
    public function getVisible()
    {
        return $this->visible;
    }
}
