<?php

namespace Admin\PanelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * Settings
 */
class Settings
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $keyValue;

    /**
     * @var string
     */
    private $textValue;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set keyValue
     *
     * @param string $keyValue
     * @return Settings
     */
    public function setKeyValue($keyValue)
    {
        $this->keyValue = $keyValue;
    
        return $this;
    }

    /**
     * Get keyValue
     *
     * @return string 
     */
    public function getKeyValue()
    {
        return $this->keyValue;
    }

    /**
     * Set textValue
     *
     * @param string $textValue
     * @return Settings
     */
    public function setTextValue($textValue)
    {
        $this->textValue = $textValue;
    
        return $this;
    }

    /**
     * Get textValue
     *
     * @return string 
     */
    public function getTextValue()
    {
        return $this->textValue;
    }
}
