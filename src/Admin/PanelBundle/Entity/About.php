<?php

namespace Admin\PanelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * About
 */
class About
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $keyValue;

    /**
     * @var string
     */
    private $titleValue;

    /**
     * @var string
     */
    private $textValue;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set keyValue
     *
     * @param string $keyValue
     * @return About
     */
    public function setKeyValue($keyValue)
    {
        $this->keyValue = $keyValue;
    
        return $this;
    }

    /**
     * Get keyValue
     *
     * @return string 
     */
    public function getKeyValue()
    {
        return $this->keyValue;
    }

    /**
     * Set titleValue
     *
     * @param string $titleValue
     * @return About
     */
    public function setTitleValue($titleValue)
    {
        $this->titleValue = $titleValue;
    
        return $this;
    }

    /**
     * Get titleValue
     *
     * @return string 
     */
    public function getTitleValue()
    {
        return $this->titleValue;
    }

    /**
     * Set textValue
     *
     * @param string $texe
     * @return About
     */
    public function setTextValue($textValue)
    {
        $this->textValue = $textValue;
    
        return $this;
    }

    /**
     * Get textValue
     *
     * @return string 
     */
    public function getTextValue()
    {
        return $this->textValue;
    }
}
