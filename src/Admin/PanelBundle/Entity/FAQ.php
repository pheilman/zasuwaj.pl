<?php

namespace Admin\PanelBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * FAQ
 */
class FAQ
{
    /**
     * @var integer
     */
    private $id;

    /**
     * @var string
     */
    private $question;

    /**
     * @var string
     */
    private $answer;

    /**
     * @var boolean
     */
    private $visible;

    /**
     * @var integer
     */
    private $faqOrder;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set question
     *
     * @param string $question
     * @return FAQ
     */
    public function setQuestion($question)
    {
        $this->question = $question;
    
        return $this;
    }

    /**
     * Get question
     *
     * @return string 
     */
    public function getQuestion()
    {
        return $this->question;
    }

    /**
     * Set answer
     *
     * @param string $answer
     * @return FAQ
     */
    public function setAnswer($answer)
    {
        $this->answer = $answer;
    
        return $this;
    }

    /**
     * Get answer
     *
     * @return string 
     */
    public function getAnswer()
    {
        return $this->answer;
    }

    /**
     * Set visible
     *
     * @param boolean $visible
     * @return FAQ
     */
    public function setVisible($visible)
    {
        $this->visible = $visible;
    
        return $this;
    }

    /**
     * Get visible
     *
     * @return boolean 
     */
    public function getVisible()
    {
        return $this->visible;
    }

    /**
     * Set faqOrder
     *
     * @param integer $faqOrder
     * @return FAQ
     */
    public function setFaqOrder($faqOrder)
    {
        $this->faqOrder = $faqOrder;
    
        return $this;
    }

    /**
     * Get faqOrder
     *
     * @return integer 
     */
    public function getFaqOrder()
    {
        return $this->faqOrder;
    }
}
