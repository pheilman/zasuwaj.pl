<div class="header">
  <div class="line">
    <h1>Edycja tekstów:</h1>
    <ul>
      <li><a href="<?php echo $view['router']->generate('homepage'); ?>">Strona główna</a></li>
      <li><span class="spacer">|</span></li>
      <li><a href="<?php echo $view['router']->generate('about'); ?>">Trenerzy (tekst)</a></li>
      <li><span class="spacer">|</span></li>
      <li><a href="<?php echo $view['router']->generate('admin_faq'); ?>">Pytania i odpowiedzi</a></li>
      <li><span class="spacer">|</span></li>
      <li><a href="<?php echo $view['router']->generate('shapr'); ?>">Oferta</a></li>
    </ul>
    <a class="logout" href="<?php echo $view['router']->generate('fos_user_security_logout'); ?>">Wyloguj</a>
  </div>
  <div class="line">
    <h1>Ustawienia:</h1>
    <ul>
      <li><a href="<?php echo $view['router']->generate('settings'); ?>">Główne</a></li>
      <li><span class="spacer">|</span></li>
      <li><a href="<?php echo $view['router']->generate('description'); ?>">Tytuły i opisy</a></li>
      <li><span class="spacer">|</span></li>
      <li><a href="<?php echo $view['router']->generate('trainer'); ?>">Trenerzy</a></li>
      <li><span class="spacer">|</span></li>
      <li><a href="<?php echo $view['router']->generate('keywords'); ?>">Footer</a></li>
    </ul>
  </div>
</div>