<?php $view->extend('::adminBase.html.php'); ?>

<?php echo $view->render('AdminPanelBundle::header.html.php'); ?>

<div class="content">
  <?php $view['slots']->output('_content') ?>
</div>

<?php echo $view->render('AdminPanelBundle::footer.html.php'); ?>