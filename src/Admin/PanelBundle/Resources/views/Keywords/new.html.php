<?php $view->extend('AdminPanelBundle::layout.html.php'); ?>

<h1>Footer - dodaj nowe słowo</h1>

<div class="button-linked-line">
  <button class="linked linked-float" href="<?php echo $view['router']->generate('keywords'); ?>">
    Powrót
  </button>
</div>

<form class="value_edit" style="margin-top: 10px;" action="<?php echo $view['router']->generate('keywords_create'); ?>" method="post" <?php echo $view['form']->enctype($form); ?>>
  <?php echo $view['form']->widget($form['_token']); ?>
  <p class="label">Słowo:</p>
  <?php echo $view['form']->widget($form['textValue']); ?>
  <p class="label">Widoczne:</p>
  <?php echo $view['form']->widget($form['visible']); ?>
  <br />
  <button type="submit">Utwórz</button>
</form>

<?php /*
<ul class="record_actions">
  <li>
    <a href="<?php echo $view['router']->generate('keywords'); ?>">
      Back to the list
    </a>
  </li>
</ul>
*/ ?>
