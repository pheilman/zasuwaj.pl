<?php $view->extend('AdminPanelBundle::layout.html.php'); ?>

<h1>Footer</h1>


<div class="button-linked-line">
  <button class="linked" href="<?php echo $view['router']->generate('keywords_new'); ?>">
    Dodaj nowe słowo
  </button>
</div>

<table class="records_list">
  <thead>
    <tr>
      <?php /*
      <th>Id</th>
      */ ?>
      <th>Słowo</th>
      <th>Widoczne</th>
      <th>Akcje</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($entities as $entity) { ?>
      <tr>
        <?php /*
        <td><a href="<?php echo $view['router']->generate('keywords_show', array('id' => $entity->getId())); ?>"><?php echo $entity->getId(); ?></a></td>
        */ ?>
        <td><?php echo $entity->getTextValue(); ?></td>
        <td><?php echo ($entity->getVisible()) ? 'tak' : 'nie'; ?></td>
        <td>
          <button class="linked" href="<?php echo $view['router']->generate('keywords_edit', array('id' => $entity->getId())); ?>">Edytuj</button>
          <button class="linked" href="<?php echo $view['router']->generate('keywords_move_up', array('id' => $entity->getId())); ?>">Przesuń do góry</button>
          <button class="linked" href="<?php echo $view['router']->generate('keywords_move_down', array('id' => $entity->getId())); ?>">Przesuń w dół</button>
          <button class="linked" href="<?php echo $view['router']->generate('keywords_toggle_visible', array('id' => $entity->getId())); ?>">Pokaż/Ukryj</button>
        </td>
      </tr>
    <?php } ?>
  </tbody>
</table>
