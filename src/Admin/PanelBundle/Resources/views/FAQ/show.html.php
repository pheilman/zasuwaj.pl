<?php $view->extend('AdminPanelBundle::layout.html.php'); ?>

<h1>Pytania i odpowiedzi</h1>


<div class="button-linked-line">
  <button class="linked linked-float" href="<?php echo $view['router']->generate('admin_faq'); ?>">
    Powrót
  </button>
  <button class="linked linked-float" href="<?php echo $view['router']->generate('admin_faq_edit', array('id' => $entity->getId())); ?>">
    Edytuj
  </button>
</div>


<div class="title" style="margin-top: 10px;">
  <?php echo $entity->getQuestion(); ?>
</div>
<div class="text">
  <?php echo $entity->getAnswer(); ?>
</div>

<?php /*
<table class="record_properties">
  <tbody>
    <tr>
      <th>Id</th>
      <td><?php echo $entity->getId(); ?></td>
    </tr>
    <tr>
      <th>Question</th>
      <td><?php echo $entity->getQuestion(); ?></td>
    </tr>
    <tr>
      <th>Answer</th>
      <td><?php echo $entity->getAnswer(); ?></td>
    </tr>
    <tr>
      <th>Visible</th>
      <td><?php echo $entity->getVisible(); ?></td>
    </tr>
    <tr>
      <th>Faqorder</th>
      <td><?php echo $entity->getFaqOrder(); ?></td>
    </tr>
  </tbody>
</table>

<ul class="record_actions">
  <li>
    <form action="<?php echo $view['router']->generate('admin_faq_delete', array('id' => $entity->getId())); ?>" method="post">
      <input type="hidden" name="_method" value="DELETE" />
      <?php echo $view['form']->widget($delete_form); ?>
      <button type="submit">Delete</button>
    </form>
  </li>
</ul>
*/ ?>