<?php $view->extend('AdminPanelBundle::layout.html.php'); ?>

<h1>Pytania i odpowiedzi - edytuj</h1>

<div class="button-linked-line">
  <form action="<?php echo $view['router']->generate('admin_faq_delete', array('id' => $entity->getId())); ?>" method="post">
    <input type="hidden" name="_method" value="DELETE" />
    <?php echo $view['form']->widget($delete_form); ?>
    <button type="submit" class="linked-float">Usuń</button>
    <button class="linked linked-float" href="<?php echo $view['router']->generate('admin_faq'); ?>">Powrót</a>
  </form>
</div>

<form class="value_edit" style="margin-top: 10px;" action="<?php echo $view['router']->generate('admin_faq_update', array('id' => $entity->getId())); ?>" method="post" <?php echo $view['form']->enctype($edit_form); ?>>
  <input type="hidden" name="_method" value="PUT" />
  <p class="label">Pytanie:</p>
  <?php echo $view['form']->widget($edit_form['question']); ?>
  <p class="label">Odpowiedź:</p>
  <?php echo $view['form']->widget($edit_form['answer']); ?>
  <br />
  <button type="submit">Edit</button>
  <div style="display: none;">
    <?php echo $view['form']->rest($edit_form); ?>
  </div>
</form>

<?php /*
<ul class="record_actions">
  <li>
    <a href="<?php echo $view['router']->generate('admin_faq'); ?>">
      Back to the list
    </a>
  </li>
  <li>
  </li>
</ul>
*/ ?>