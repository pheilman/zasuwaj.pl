<?php $view->extend('AdminPanelBundle::layout.html.php'); ?>

<h1>Pytania i odpowiedzi - dodaj nowe</h1>

<div class="button-linked-line">
  <button class="linked linked-float" href="<?php echo $view['router']->generate('admin_faq'); ?>">
    Powrót
  </button>
</div>

<form class="value_edit" style="margin-top: 10px;" action="<?php echo $view['router']->generate('admin_faq_create'); ?>" method="post" <?php echo $view['form']->enctype($form); ?>>
  <?php echo $view['form']->widget($form['_token']); ?>
  <p class="label">Pytanie:</p>
  <?php echo $view['form']->widget($form['question']); ?>
  <p class="label">Odpowiedź</p>
  <?php echo $view['form']->widget($form['answer']); ?>
  <p class="label">Widoczne:</p>
  <?php echo $view['form']->widget($form['visible']); ?>
  <br />
  <button type="submit">Utwórz</button>
</form>

<?php /*
<ul class="record_actions">
  <li>
    <a href="<?php echo $view['router']->generate('admin_faq'); ?>">
      Back to the list
    </a>
  </li>
</ul>
*/ ?>
