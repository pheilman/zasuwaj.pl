<?php $view->extend('AdminPanelBundle::layout.html.php'); ?>

<h1>Pytania i odpowiedzi</h1>


<div class="button-linked-line">
  <button class="linked" href="<?php echo $view['router']->generate('admin_faq_new'); ?>">
    Dodaj nowe
  </button>
</div>

<table class="records_list">
  <thead>
    <tr>
      <?php /*
      <th>Id</th>
      */ ?>
      <th>Pytanie</th>
      <th>Widoczne</th>
      <th rowspan="2">Akcje</th>
    </tr>
    <tr>
      <th colspan="2">Odpowiedź</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($entities as $entity) { ?>
      <tr>
        <?php /*
        <td><a href="<?php echo $view['router']->generate('admin_faq_show', array('id' => $entity->getId())); ?>"><?php echo $entity->getId(); ?></a></td>
        */ ?>
        <td><?php echo $entity->getQuestion(); ?></td>
        <td><?php echo ($entity->getVisible()) ? 'tak' : 'nie'; ?></td>
        <td rowspan="2">
          <button class="linked" href="<?php echo $view['router']->generate('admin_faq_edit', array('id' => $entity->getId())); ?>">Edytuj</button>
          <button class="linked" href="<?php echo $view['router']->generate('admin_faq_move_up', array('id' => $entity->getId())); ?>">Przesuń do góry</button>
          <button class="linked" href="<?php echo $view['router']->generate('admin_faq_move_down', array('id' => $entity->getId())); ?>">Przesuń w dół</button>
          <button class="linked" href="<?php echo $view['router']->generate('admin_faq_toggle_visible', array('id' => $entity->getId())); ?>">Pokaż/Ukryj</button>
        </td>
      </tr>
      <tr>
        <td colspan="2"><?php echo $entity->getAnswer(); ?></td>
      </tr>
    <?php } ?>
  </tbody>
</table>
