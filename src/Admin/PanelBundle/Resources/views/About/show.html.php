<?php $view->extend('AdminPanelBundle::layout.html.php'); ?>

<h1>O nas</h1>

<div class="button-linked-line">
  <button class="linked linked-float" href="<?php echo $view['router']->generate('about'); ?>">
    Powrót
  </button>
  <button class="linked linked-float" href="<?php echo $view['router']->generate('about_edit', array('id' => $entity->getId())); ?>">
    Edytuj
  </button>
</div>

<p class="shown"><span class="bold">Wyświetlone dla: </span><?php echo $entity->getKeyValue(); ?></p>

<div class="title">
  <?php echo $entity->getTitleValue(); ?>
</div>
<div class="text">
  <?php echo $entity->getTextValue(); ?>
</div>

<?php /*
<table class="record_properties">
  <tbody>
    <tr>
      <th>Id</th>
      <td><?php echo $entity->getId(); ?></td>
    </tr>
    <tr>
      <th>Keyvalue</th>
      <td><?php echo $entity->getKeyValue(); ?></td>
    </tr>
    <tr>
      <th>Titlevalue</th>
      <td><?php echo $entity->getTitleValue(); ?></td>
    </tr>
    <tr>
      <th>Textvalue</th>
      <td><?php echo $entity->getTextValue(); ?></td>
    </tr>
  </tbody>
</table>

<ul class="record_actions">
  <li>
    <form action="<?php echo $view['router']->generate('about_delete', array('id' => $entity->getId())); ?>" method="post">
      <input type="hidden" name="_method" value="DELETE" />
      <?php echo $view['form']->widget($delete_form); ?>
      <button type="submit">Delete</button>
    </form>
  </li>
</ul>
*/ ?>
