<?php $view->extend('AdminPanelBundle::layout.html.php'); ?>

<h1>About creation</h1>

<form action="<?php echo $view['router']->generate('about_create'); ?>" method="post" <?php echo $view['form']->enctype($form); ?>>
  <?php echo $view['form']->widget($form); ?>
  <p>
    <button type="submit">Create</button>
  </p>
</form>

<ul class="record_actions">
  <li>
    <a href="<?php echo $view['router']->generate('about'); ?>">
      Back to the list
    </a>
  </li>
</ul>
