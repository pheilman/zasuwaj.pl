<?php $view->extend('AdminPanelBundle::layout.html.php'); ?>

<h1>Ustawienia - edytuj</h1>

<a href="<?php echo $view['router']->generate('description'); ?>">
  Powrót
</a>

<p class="shown"><span class="bold">Strona: </span><?php echo $edit_value; ?></p>

<form class="value_edit" action="<?php echo $view['router']->generate('description_update', array('id' => $entity->getId())); ?>" method="post" <?php echo $view['form']->enctype($edit_form); ?>>
  <input type="hidden" name="_method" value="PUT" />
  <p class="label">Tytuł:</p>
  <?php echo $view['form']->widget($edit_form['title']); ?>
  <p class="label">Opis:</p>
  <?php echo $view['form']->widget($edit_form['description']); ?>
  <br />
  <button type="submit">Edytuj</button>
  <div style="display: none;">
    <?php echo $view['form']->rest($edit_form); ?>
  </div>
</form>

<?php /*
<ul class="record_actions">
  <li>
    <form action="<?php echo $view['router']->generate('description_delete', array('id' => $entity->getId())); ?>" method="post">
      <input type="hidden" name="_method" value="DELETE" />
      <?php echo $view['form']->widget($delete_form); ?>
      <button type="submit">Delete</button>
    </form>
  </li>
</ul>
*/ ?>