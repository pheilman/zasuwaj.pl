<?php $view->extend('AdminPanelBundle::layout.html.php'); ?>

<h1>Tytuły i opisy</h1>

<table class="records_list">
  <thead>
    <tr>
      <?php /*
      <th>Id</th>
      */ ?>
      <th>Strona</th>
      <th>Tytuł</th>
      <th>Opis</th>
      <th>Akcje</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($entities as $entity) { ?>
      <tr>
        <?php /*
        <td><a href="<?php echo $view['router']->generate('settings_show', array('id' => $entity->getId())); ?>"><?php echo $entity->getId(); ?></a></td>
        */ ?>
        <td><?php echo $entity->getPageName(); ?></td>
        <td><?php echo $entity->getTitle(); ?></td>
        <td><?php echo $entity->getDescription(); ?></td>
        <td>
          <?php /*<a href="<?php echo $view['router']->generate('description_show', array('id' => $entity->getId())); ?>">show</a> */ ?>
          <button class="linked" href="<?php echo $view['router']->generate('description_edit', array('id' => $entity->getId())); ?>">Edytuj</button>
        </td>
      </tr>
    <?php } ?>
  </tbody>
</table>

<?php /*
<ul>
  <li>
    <a href="<?php echo $view['router']->generate('settings_new'); ?>">
      Create a new entry
    </a>
  </li>
</ul>
*/ ?>