
<?php $view->extend('AdminPanelBundle::layout.html.php'); ?>

<h1>Oferta</h1>

<table class="records_list">
  <thead>
    <tr>
      <?php /*
      <th>Id</th>
      */ ?>
      <th>Klucz</th>
      <th>Tytuł</th>
      <th rowspan="2">Akcja</th>
    </tr>
    <tr>
      <th colspan="2">Tekst</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($entities as $entity) { ?>
      <tr>
        <?php /*
        <td><a href="<?php echo $view['router']->generate('shapr_show', array('id' => $entity->getId())); ?>"><?php echo $entity->getId(); ?></a></td>
        */ ?>
        <td><?php echo $entity->getKeyValue(); ?></td>
        <td><?php echo $entity->getTitleValue(); ?></td>
        <td rowspan="2">
          <button class="linked" href="<?php echo $view['router']->generate('shapr_show', array('id' => $entity->getId())); ?>">Pokaż</button>
          <button class="linked" href="<?php echo $view['router']->generate('shapr_edit', array('id' => $entity->getId())); ?>">Edytuj</button>
        </td>
      </tr>
      <tr>
        <td colspan="2"><?php echo $entity->getTextValue(); ?></td>
      </tr>
    <?php } ?>
  </tbody>
</table>

<?php /*
{% extends '::base.html.twig' %}

{% block body -%}
    <h1>ShAPr list</h1>

    <table class="records_list">
        <thead>
            <tr>
                <th>Id</th>
                <th>Keyvalue</th>
                <th>Titlevalue</th>
                <th>Textvalue</th>
                <th>Actions</th>
            </tr>
        </thead>
        <tbody>
        {% for entity in entities %}
            <tr>
                <td><a href="{{ path('shapr_show', { 'id': entity.id }) }}">{{ entity.id }}</a></td>
                <td>{{ entity.keyValue }}</td>
                <td>{{ entity.titleValue }}</td>
                <td>{{ entity.textValue }}</td>
                <td>
                <ul>
                    <li>
                        <a href="{{ path('shapr_show', { 'id': entity.id }) }}">show</a>
                    </li>
                    <li>
                        <a href="{{ path('shapr_edit', { 'id': entity.id }) }}">edit</a>
                    </li>
                </ul>
                </td>
            </tr>
        {% endfor %}
        </tbody>
    </table>

        <ul>
        <li>
            <a href="{{ path('shapr_new') }}">
                Create a new entry
            </a>
        </li>
    </ul>
    {% endblock %}
*/ ?>