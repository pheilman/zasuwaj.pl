<?php $view->extend('AdminPanelBundle::layout.html.php'); ?>

<h1>Oferta - edytuj</h1>

<div class="button-linked-line">
  <button class="linked linked-float" href="<?php echo $view['router']->generate('shapr'); ?>">
    Powrót
  </button>
</div>


<p class="shown"><span class="bold">Zmieniana wartość: </span><?php echo $edit_value; ?></p>

<form class="value_edit" action="<?php echo $view['router']->generate('shapr_update', array('id' => $entity->getId())); ?>" method="post" <?php echo $view['form']->enctype($edit_form); ?>>
  <input type="hidden" name="_method" value="PUT" />
  <p class="label">Tytuł:</p>
  <?php echo $view['form']->widget($edit_form['titleValue']); ?>
  <p class="label">Tekst:</p>
  <?php echo $view['form']->widget($edit_form['textValue']); ?>
  <br />
  <button type="submit">Edytuj</button>
  <div style="display: none;">
    <?php echo $view['form']->rest($edit_form); ?>
  </div>
</form>

<?php /*
{% extends '::base.html.twig' %}

{% block body -%}
    <h1>ShAPr edit</h1>

    <form action="{{ path('shapr_update', { 'id': entity.id }) }}" method="post" {{ form_enctype(edit_form) }}>
        <input type="hidden" name="_method" value="PUT" />
        {{ form_widget(edit_form) }}
        <p>
            <button type="submit">Edit</button>
        </p>
    </form>

        <ul class="record_actions">
    <li>
        <a href="{{ path('shapr') }}">
            Back to the list
        </a>
    </li>
    <li>
        <form action="{{ path('shapr_delete', { 'id': entity.id }) }}" method="post">
            <input type="hidden" name="_method" value="DELETE" />
            {{ form_widget(delete_form) }}
            <button type="submit">Delete</button>
        </form>
    </li>
</ul>
{% endblock %}
*/ ?>