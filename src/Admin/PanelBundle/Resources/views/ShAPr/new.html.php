<?php $view->extend('AdminPanelBundle::layout.html.php'); ?>

<h1>Oferta - nowy</h1>

<form action="<?php echo $view['router']->generate('shapr_create'); ?>" method="post" <?php echo $view['form']->enctype($form); ?>>
  <?php echo $view['form']->widget($form); ?>
  <p>
    <button type="submit">Create</button>
  </p>
</form>

<ul class="record_actions">
  <li>
    <a href="<?php echo $view['router']->generate('shapr'); ?>">
      Back to the list
    </a>
  </li>
</ul>

<?php /*
{% extends '::base.html.twig' %}

{% block body -%}
    <h1>ShAPr creation</h1>

    <form action="{{ path('shapr_create') }}" method="post" {{ form_enctype(form) }}>
        {{ form_widget(form) }}
        <p>
            <button type="submit">Create</button>
        </p>
    </form>

        <ul class="record_actions">
    <li>
        <a href="{{ path('shapr') }}">
            Back to the list
        </a>
    </li>
</ul>
{% endblock %}
*/ ?>