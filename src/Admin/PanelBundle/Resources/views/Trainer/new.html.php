<?php $view->extend('AdminPanelBundle::layout.html.php'); ?>

<h1>Trener - dodaj nowego</h1>

<div class="button-linked-line">
  <button class="linked linked-float" href="<?php echo $view['router']->generate('trainer'); ?>">
    Powrót
  </button>
</div>

<form class="value_edit" style="margin-top: 10px;" action="<?php echo $view['router']->generate('trainer_create'); ?>" method="post" <?php echo $view['form']->enctype($form); ?>>
  <?php echo $view['form']->widget($form['_token']); ?>
  <p class="label">Imię i nazwisko:</p>
  <?php echo $view['form']->widget($form['name']); ?>
  <p class="label">Informacje:</p>
  <?php echo $view['form']->widget($form['about']); ?>
  <p class="label">Widoczny:</p>
  <?php echo $view['form']->widget($form['visible']); ?>
  <p class="label">Zdjęcie:</p>
  <?php echo $view['form']->widget($form['file']); ?>
  <br />
  <button type="submit">Utwórz</button>
</form>

<?php /*
<ul class="record_actions">
  <li>
    <a href="<?php echo $view['router']->generate('trainer'); ?>">
      Back to the list
    </a>
  </li>
</ul>
*/ ?>
