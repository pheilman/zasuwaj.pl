<?php $view->extend('AdminPanelBundle::layout.html.php'); ?>

<h1>Trenerzy</h1>


<div class="button-linked-line">
  <button class="linked linked-float" href="<?php echo $view['router']->generate('trainer'); ?>">
    Powrót
  </button>
  <button class="linked linked-float" href="<?php echo $view['router']->generate('trainer_edit', array('id' => $entity->getId())); ?>">
    Edytuj
  </button>
</div>


<div class="title" style="margin-top: 10px;">
  <?php echo $entity->getName(); ?>
</div>
<div class="text">
  <?php echo $entity->getAbout(); ?>
</div>