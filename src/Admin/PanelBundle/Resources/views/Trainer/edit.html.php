<?php $view->extend('AdminPanelBundle::layout.html.php'); ?>

<h1>Trener - edytuj</h1>

<div class="button-linked-line">
  <form action="<?php echo $view['router']->generate('trainer_delete', array('id' => $entity->getId())); ?>" method="post">
    <input type="hidden" name="_method" value="DELETE" />
    <?php echo $view['form']->widget($delete_form); ?>
    <button type="submit" class="linked-float">Usuń</button>
    <button class="linked linked-float" href="<?php echo $view['router']->generate('trainer'); ?>">Powrót</a>
  </form>
</div>

<form class="value_edit" style="margin-top: 10px;" action="<?php echo $view['router']->generate('trainer_update', array('id' => $entity->getId())); ?>" method="post" <?php echo $view['form']->enctype($edit_form); ?>>
  <input type="hidden" name="_method" value="PUT" />
  <?php echo $view['form']->widget($edit_form['_token']); ?>
  <p class="label">Imię i nazwisko:</p>
  <?php echo $view['form']->widget($edit_form['name']); ?>
  <p class="label">Informacje:</p>
  <?php echo $view['form']->widget($edit_form['about']); ?>
  <p class="label">Widoczny:</p>
  <?php echo $view['form']->widget($edit_form['visible']); ?>
  <p class="label">Zdjęcie:</p>
  <?php echo $view['form']->widget($edit_form['file']); ?>
  <br />
  <button type="submit">Edit</button>
</form>

<?php /*
<ul class="record_actions">
  <li>
    <a href="<?php echo $view['router']->generate('trainer'); ?>">
      Back to the list
    </a>
  </li>
  <li>
  </li>
</ul>
*/ ?>