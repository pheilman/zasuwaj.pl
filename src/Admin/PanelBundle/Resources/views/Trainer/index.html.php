<?php $view->extend('AdminPanelBundle::layout.html.php'); ?>

<h1>Trenerzy</h1>


<div class="button-linked-line">
  <button class="linked" href="<?php echo $view['router']->generate('trainer_new'); ?>">
    Dodaj nowego
  </button>
</div>

<table class="records_list">
  <thead>
    <tr>
      <?php /*
      <th>Id</th>
      */ ?>
      <th rowspan="2">Zdjęcie</th>
      <th>Imię i nazwisko</th>
      <th>Widoczny</th>
      <th rowspan="2">Akcje</th>
    </tr>
    <tr>
      <th colspan="2">Informacje</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($entities as $entity) { ?>
      <tr>
        <?php /*
        <td><a href="<?php echo $view['router']->generate('trainer_show', array('id' => $entity->getId())); ?>"><?php echo $entity->getId(); ?></a></td>
        */ ?>
        <td rowspan="2"><img src="<?php echo $this['imagine']->filter('/'.$entity->getWebPath().'/'.$entity->getPhoto(), 'trainer_thumb'); ?>" /></td>
        <td><?php echo $entity->getName(); ?></td>
        <td><?php echo ($entity->getVisible()) ? 'tak' : 'nie'; ?></td>
        <td rowspan="2">
          <?php /*<button class="linked" href="<?php echo $view['router']->generate('trainer_show', array('id' => $entity->getId())); ?>">Pokaż</button>*/ ?>
          <button class="linked" href="<?php echo $view['router']->generate('trainer_edit', array('id' => $entity->getId())); ?>">Edytuj</button>
          <button class="linked" href="<?php echo $view['router']->generate('trainer_move_up', array('id' => $entity->getId())); ?>">Przesuń do góry</button>
          <button class="linked" href="<?php echo $view['router']->generate('trainer_move_down', array('id' => $entity->getId())); ?>">Przesuń w dół</button>
          <button class="linked" href="<?php echo $view['router']->generate('trainer_toggle_visible', array('id' => $entity->getId())); ?>">Pokaż/Ukryj</button>
        </td>
      </tr>
      <tr>
        <td colspan="2"><?php echo $entity->getAbout(); ?></td>
      </tr>
    <?php } ?>
  </tbody>
</table>
