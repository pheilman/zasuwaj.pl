<?php $view->extend('AdminPanelBundle::layout.html.php'); ?>

<h1>Strona główna</h1>

<table class="records_list">
  <thead>
    <tr>
      <?php /*
      <th>Id</th>
      */ ?>
      <th>Klucz</th>
      <th>Tytuł</th>
      <th rowspan="2">Akcja</th>
    </tr>
    <tr>
      <th colspan="2">Tekst</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($entities as $entity) { ?>
      <tr>
        <?php /*
        <td><a href="<?php echo $view['router']->generate('homepage_show', array('id' => $entity->getId())); ?>"><?php echo $entity->getId(); ?></a></td>
        */ ?>
        <td><?php echo $entity->getKeyValue(); ?></td>
        <td><?php echo $entity->getTitleValue(); ?></td>
        <td rowspan="2">
          <button class="linked" href="<?php echo $view['router']->generate('homepage_show', array('id' => $entity->getId())); ?>">Pokaż</button>
          <button class="linked" href="<?php echo $view['router']->generate('homepage_edit', array('id' => $entity->getId())); ?>">Edytuj</button>
        </td>
      </tr>
      <tr>
        <td colspan="2"><?php echo $entity->getTextValue(); ?></td>
      </tr>
    <?php } ?>
  </tbody>
</table>

<?php /*
<ul>
  <li>
    <a href="<?php echo $view['router']->generate('homepage_new'); ?>">
      Create a new entry
    </a>
  </li>
</ul>
*/ ?>
