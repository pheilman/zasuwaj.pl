<?php $view->extend('AdminPanelBundle::layout.html.php'); ?>

<h1>Ustawienia</h1>

<table class="records_list">
  <thead>
    <tr>
      <?php /*
      <th>Id</th>
      */ ?>
      <th>Klucz</th>
      <th>Wartość</th>
      <th>Akcje</th>
    </tr>
  </thead>
  <tbody>
    <?php foreach($entities as $entity) { ?>
      <tr>
        <?php /*
        <td><a href="<?php echo $view['router']->generate('settings_show', array('id' => $entity->getId())); ?>"><?php echo $entity->getId(); ?></a></td>
        */ ?>
        <td><?php echo $entity->getKeyValue(); ?></td>
        <td><?php echo $entity->getTextValue(); ?></td>
        <td>
          <?php /*<a href="<?php echo $view['router']->generate('settings_show', array('id' => $entity->getId())); ?>">show</a> */ ?>
          <button class="linked" href="<?php echo $view['router']->generate('settings_edit', array('id' => $entity->getId())); ?>">Edytuj</button>
        </td>
      </tr>
    <?php } ?>
  </tbody>
</table>

<?php /*
<ul>
  <li>
    <a href="<?php echo $view['router']->generate('settings_new'); ?>">
      Create a new entry
    </a>
  </li>
</ul>
*/ ?>

