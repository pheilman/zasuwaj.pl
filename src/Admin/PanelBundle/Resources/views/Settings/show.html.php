<?php $view->extend('AdminPanelBundle::layout.html.php'); ?>

<h1>Ustawienia</h1>

<table class="record_properties">
  <tbody>
    <?php /*
    <tr>
      <th>Id</th>
      <td><?php echo $entity->getId(); ?></td>
    </tr>
    */ ?>
    <tr>
      <th>Keyvalue</th>
      <td><?php echo $entity->getKeyValue(); ?></td>
    </tr>
    <tr>
      <th>Textvalue</th>
      <td><?php echo $entity->getTextValue(); ?></td>
    </tr>
  </tbody>
</table>

<ul class="record_actions">
  <li>
    <a href="<?php echo $view['router']->generate('settings'); ?>">
      Back to the list
    </a>
  </li>
  <li>
    <a href="<?php echo $view['router']->generate('settings_edit', array('id' => $entity->getId())); ?>">
      Edit
    </a>
  </li>
  <?php /*
  <li>
    <form action="<?php echo $view['router']->generate('settings_delete', array('id' => $entity->getId())); ?>" method="post">
      <input type="hidden" name="_method" value="DELETE" />
      <?php echo $view['form']->widget($delete_form); ?>
      <button type="submit">Delete</button>
    </form>
  </li>
  */ ?>
</ul>
