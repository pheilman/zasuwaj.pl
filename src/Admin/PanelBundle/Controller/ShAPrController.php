<?php

namespace Admin\PanelBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Admin\PanelBundle\Entity\ShAPr;
use Admin\PanelBundle\Form\ShAPrType;

/**
 * ShAPr controller.
 *
 */
class ShAPrController extends Controller
{
    /**
     * Lists all ShAPr entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AdminPanelBundle:ShAPr')->findAll();

        return $this->render('AdminPanelBundle:ShAPr:index.html.php', array(
            'entities' => $entities,
        ));
    }

    /**
     * Creates a new ShAPr entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new ShAPr();
        $form = $this->createForm(new ShAPrType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('shapr_show', array('id' => $entity->getId())));
        }

        return $this->render('AdminPanelBundle:ShAPr:new.html.php', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new ShAPr entity.
     *
     */
    public function newAction()
    {
        $entity = new ShAPr();
        $form   = $this->createForm(new ShAPrType(), $entity);

        return $this->render('AdminPanelBundle:ShAPr:new.html.php', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a ShAPr entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminPanelBundle:ShAPr')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ShAPr entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminPanelBundle:ShAPr:show.html.php', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing ShAPr entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminPanelBundle:ShAPr')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ShAPr entity.');
        }

        $editForm = $this->createForm(new ShAPrType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $data = $editForm->getData()->getKeyValue();

        return $this->render('AdminPanelBundle:ShAPr:edit.html.php', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'edit_value'  => $data,
        ));
    }

    /**
     * Edits an existing ShAPr entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminPanelBundle:ShAPr')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find ShAPr entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new ShAPrType(), $entity);
        $editForm->bind($request);
        $data = $editForm->getData()->getKeyValue();

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('shapr_edit', array('id' => $id)));
        }

        return $this->render('AdminPanelBundle:ShAPr:edit.html.php', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'edit_value'  => $data,
        ));
    }

    /**
     * Deletes a ShAPr entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdminPanelBundle:ShAPr')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find ShAPr entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('shapr'));
    }

    /**
     * Creates a form to delete a ShAPr entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
