<?php

namespace Admin\PanelBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Admin\PanelBundle\Entity\Keywords;
use Admin\PanelBundle\Form\KeywordsType;

/**
 * Keywords controller.
 *
 */
class KeywordsController extends Controller
{
    /**
     * Lists all Keywords entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AdminPanelBundle:Keywords')->findAllOrdered();

        return $this->render('AdminPanelBundle:Keywords:index.html.php', array(
            'entities' => $entities,
        ));
    }

    /**
     * Creates a new Keywords entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new Keywords();
        $form = $this->createForm(new KeywordsType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $entity->setOrderValue($entity->getId());
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('keywords', array('id' => $entity->getId())));
        }

        return $this->render('AdminPanelBundle:Keywords:new.html.php', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new Keywords entity.
     *
     */
    public function newAction()
    {
        $entity = new Keywords();
        $form   = $this->createForm(new KeywordsType(), $entity);

        return $this->render('AdminPanelBundle:Keywords:new.html.php', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Keywords entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminPanelBundle:Keywords')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Keywords entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminPanelBundle:Keywords:show.html.php', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Keywords entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminPanelBundle:Keywords')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Keywords entity.');
        }

        $editForm = $this->createForm(new KeywordsType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminPanelBundle:Keywords:edit.html.php', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Keywords entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminPanelBundle:Keywords')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Keywords entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new KeywordsType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('keywords_edit', array('id' => $id)));
        }

        return $this->render('AdminPanelBundle:Keywords:edit.html.php', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Keywords entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdminPanelBundle:Keywords')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Keywords entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('keywords'));
    }

    /**
     * Creates a form to delete a Keywords entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    public function moveUpAction(Request $request, $id)
    { 
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminPanelBundle:Keywords')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Keywords entity.');
        }

        $em->getRepository('AdminPanelBundle:Keywords')->moveUp($entity->getId());

        return $this->redirect($this->generateUrl('keywords'));   
    }

    public function moveDownAction(Request $request, $id)
    { 
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminPanelBundle:Keywords')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Keywords entity.');
        }

        $em->getRepository('AdminPanelBundle:Keywords')->moveDown($entity->getId());

        return $this->redirect($this->generateUrl('keywords'));   
    }

    public function toggleVisibleAction(Request $request, $id)
    { 
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminPanelBundle:Keywords')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Keywords entity.');
        }

        $em->getRepository('AdminPanelBundle:Keywords')->toggleVisible($entity->getId());

        return $this->redirect($this->generateUrl('keywords'));   
    }
}
