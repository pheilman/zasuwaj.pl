<?php

namespace Admin\PanelBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Admin\PanelBundle\Entity\Homepage;
use Admin\PanelBundle\Form\HomepageType;

/**
 * Homepage controller.
 *
 */
class HomepageController extends Controller
{
    /**
     * Lists all Homepage entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AdminPanelBundle:Homepage')->findAll();

        return $this->render('AdminPanelBundle:Homepage:index.html.php', array(
            'entities' => $entities,
        ));
    }

    /**
     * Creates a new Homepage entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new Homepage();
        $form = $this->createForm(new HomepageType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('homepage_show', array('id' => $entity->getId())));
        }

        return $this->render('AdminPanelBundle:Homepage:new.html.php', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new Homepage entity.
     *
     */
    public function newAction()
    {
        $entity = new Homepage();
        $form   = $this->createForm(new HomepageType(), $entity);

        return $this->render('AdminPanelBundle:Homepage:new.html.php', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Homepage entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminPanelBundle:Homepage')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Homepage entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminPanelBundle:Homepage:show.html.php', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Homepage entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminPanelBundle:Homepage')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Homepage entity.');
        }

        $editForm = $this->createForm(new HomepageType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $data = $editForm->getData()->getKeyValue();

        return $this->render('AdminPanelBundle:Homepage:edit.html.php', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'edit_value'  => $data,
        ));
    }

    /**
     * Edits an existing Homepage entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminPanelBundle:Homepage')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Homepage entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new HomepageType(), $entity);
        $editForm->bind($request);

        $data = $editForm->getData()->getKeyValue();

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('homepage_edit', array('id' => $id)));
        }

        return $this->render('AdminPanelBundle:Homepage:edit.html.php', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'edit_value'  => $data,
        ));
    }

    /**
     * Deletes a Homepage entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdminPanelBundle:Homepage')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Homepage entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('homepage'));
    }

    /**
     * Creates a form to delete a Homepage entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
