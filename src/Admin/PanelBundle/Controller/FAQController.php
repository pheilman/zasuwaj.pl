<?php

namespace Admin\PanelBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Admin\PanelBundle\Entity\FAQ;
use Admin\PanelBundle\Form\FAQType;

/**
 * FAQ controller.
 *
 */
class FAQController extends Controller
{
    /**
     * Lists all FAQ entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AdminPanelBundle:FAQ')->findAllOrderedByFaqOrder();

        return $this->render('AdminPanelBundle:FAQ:index.html.php', array(
            'entities' => $entities,
        ));
    }

    /**
     * Creates a new FAQ entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new FAQ();
        $form = $this->createForm(new FAQType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $entity->setFaqOrder($entity->getId());
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_faq'));
        }

        return $this->render('AdminPanelBundle:FAQ:new.html.php', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new FAQ entity.
     *
     */
    public function newAction()
    {
        $entity = new FAQ();
        $form   = $this->createForm(new FAQType(), $entity);

        return $this->render('AdminPanelBundle:FAQ:new.html.php', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a FAQ entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminPanelBundle:FAQ')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find FAQ entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminPanelBundle:FAQ:show.html.php', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing FAQ entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminPanelBundle:FAQ')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find FAQ entity.');
        }

        $editForm = $this->createForm(new FAQType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminPanelBundle:FAQ:edit.html.php', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing FAQ entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminPanelBundle:FAQ')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find FAQ entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new FAQType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('admin_faq_edit', array('id' => $id)));
        }

        return $this->render('AdminPanelBundle:FAQ:edit.html.php', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a FAQ entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdminPanelBundle:FAQ')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find FAQ entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('admin_faq'));
    }

    /**
     * Creates a form to delete a FAQ entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    public function moveUpAction(Request $request, $id)
    { 
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminPanelBundle:FAQ')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find FAQ entity.');
        }

        $em->getRepository('AdminPanelBundle:FAQ')->moveUp($entity->getId());

        return $this->redirect($this->generateUrl('admin_faq'));   
    }

    public function moveDownAction(Request $request, $id)
    { 
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminPanelBundle:FAQ')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find FAQ entity.');
        }

        $em->getRepository('AdminPanelBundle:FAQ')->moveDown($entity->getId());

        return $this->redirect($this->generateUrl('admin_faq'));   
    }

    public function toggleVisibleAction(Request $request, $id)
    { 
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminPanelBundle:FAQ')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find FAQ entity.');
        }

        $em->getRepository('AdminPanelBundle:FAQ')->toggleVisible($entity->getId());

        return $this->redirect($this->generateUrl('admin_faq'));   
    }
}
