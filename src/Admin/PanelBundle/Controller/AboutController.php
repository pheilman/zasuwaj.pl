<?php

namespace Admin\PanelBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Admin\PanelBundle\Entity\About;
use Admin\PanelBundle\Form\AboutType;

/**
 * About controller.
 *
 */
class AboutController extends Controller
{
    /**
     * Lists all About entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AdminPanelBundle:About')->findAll();

        return $this->render('AdminPanelBundle:About:index.html.php', array(
            'entities' => $entities,
        ));
    }

    /**
     * Creates a new About entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new About();
        $form = $this->createForm(new AboutType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('about_show', array('id' => $entity->getId())));
        }

        return $this->render('AdminPanelBundle:About:new.html.php', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new About entity.
     *
     */
    public function newAction()
    {
        $entity = new About();
        $form   = $this->createForm(new AboutType(), $entity);

        return $this->render('AdminPanelBundle:About:new.html.php', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a About entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminPanelBundle:About')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find About entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminPanelBundle:About:show.html.php', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing About entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminPanelBundle:About')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find About entity.');
        }

        $editForm = $this->createForm(new AboutType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $data = $editForm->getData()->getKeyValue();

        return $this->render('AdminPanelBundle:About:edit.html.php', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'edit_value'  => $data,
        ));
    }

    /**
     * Edits an existing About entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminPanelBundle:About')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find About entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new AboutType(), $entity);
        $editForm->bind($request);

        $data = $editForm->getData()->getKeyValue();

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('about_edit', array('id' => $id)));
        }

        return $this->render('AdminPanelBundle:About:edit.html.php', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'edit_value'  => $data,
        ));
    }

    /**
     * Deletes a About entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdminPanelBundle:About')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find About entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('about'));
    }

    /**
     * Creates a form to delete a About entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
