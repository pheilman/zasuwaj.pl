<?php

namespace Admin\PanelBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Admin\PanelBundle\Entity\Trainer;
use Admin\PanelBundle\Form\TrainerType;

/**
 * Trainer controller.
 *
 */
class TrainerController extends Controller
{
    /**
     * Lists all Trainer entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AdminPanelBundle:Trainer')->findAllOrdered();

        return $this->render('AdminPanelBundle:Trainer:index.html.php', array(
            'entities' => $entities,
        ));
    }

    /**
     * Creates a new Trainer entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new Trainer();
        $form = $this->createForm(new TrainerType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            $entity->setPeopleOrder($entity->getId());
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('trainer'));
        }

        return $this->render('AdminPanelBundle:Trainer:new.html.php', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new Trainer entity.
     *
     */
    public function newAction()
    {
        $entity = new Trainer();
        $form   = $this->createForm(new TrainerType(), $entity);

        return $this->render('AdminPanelBundle:Trainer:new.html.php', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Trainer entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminPanelBundle:Trainer')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Trainer entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminPanelBundle:Trainer:show.html.php', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Displays a form to edit an existing Trainer entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminPanelBundle:Trainer')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Trainer entity.');
        }

        $editForm = $this->createForm(new TrainerType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminPanelBundle:Trainer:edit.html.php', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Edits an existing Trainer entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminPanelBundle:Trainer')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Trainer entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new TrainerType(), $entity);
        $editForm->bind($request);

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('trainer_edit', array('id' => $id)));
        }

        return $this->render('AdminPanelBundle:Trainer:edit.html.php', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
        ));
    }

    /**
     * Deletes a Trainer entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdminPanelBundle:Trainer')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Trainer entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('trainer'));
    }

    /**
     * Creates a form to delete a Trainer entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }

    public function moveUpAction(Request $request, $id)
    { 
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminPanelBundle:Trainer')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Trainer entity.');
        }

        $em->getRepository('AdminPanelBundle:Trainer')->moveUp($entity->getId());

        return $this->redirect($this->generateUrl('trainer'));   
    }

    public function moveDownAction(Request $request, $id)
    { 
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminPanelBundle:Trainer')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Trainer entity.');
        }

        $em->getRepository('AdminPanelBundle:Trainer')->moveDown($entity->getId());

        return $this->redirect($this->generateUrl('trainer'));   
    }

    public function toggleVisibleAction(Request $request, $id)
    { 
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminPanelBundle:Trainer')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Trainer entity.');
        }

        $em->getRepository('AdminPanelBundle:Trainer')->toggleVisible($entity->getId());

        return $this->redirect($this->generateUrl('trainer'));   
    }
}
