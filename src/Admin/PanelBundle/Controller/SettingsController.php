<?php

namespace Admin\PanelBundle\Controller;

use Symfony\Component\HttpFoundation\Request;
use Symfony\Bundle\FrameworkBundle\Controller\Controller;

use Admin\PanelBundle\Entity\Settings;
use Admin\PanelBundle\Form\SettingsType;

/**
 * Settings controller.
 *
 */
class SettingsController extends Controller
{
    /**
     * Lists all Settings entities.
     *
     */
    public function indexAction()
    {
        $em = $this->getDoctrine()->getManager();

        $entities = $em->getRepository('AdminPanelBundle:Settings')->findAll();

        return $this->render('AdminPanelBundle:Settings:index.html.php', array(
            'entities' => $entities,
        ));
    }

    /**
     * Creates a new Settings entity.
     *
     */
    public function createAction(Request $request)
    {
        $entity  = new Settings();
        $form = $this->createForm(new SettingsType(), $entity);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('settings_show', array('id' => $entity->getId())));
        }

        return $this->render('AdminPanelBundle:Settings:new.html.php', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Displays a form to create a new Settings entity.
     *
     */
    public function newAction()
    {
        $entity = new Settings();
        $form   = $this->createForm(new SettingsType(), $entity);

        return $this->render('AdminPanelBundle:Settings:new.html.php', array(
            'entity' => $entity,
            'form'   => $form->createView(),
        ));
    }

    /**
     * Finds and displays a Settings entity.
     *
     */
    public function showAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminPanelBundle:Settings')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Settings entity.');
        }

        $deleteForm = $this->createDeleteForm($id);

        return $this->render('AdminPanelBundle:Settings:show.html.php', array(
            'entity'      => $entity,
            'delete_form' => $deleteForm->createView(),        ));
    }

    /**
     * Displays a form to edit an existing Settings entity.
     *
     */
    public function editAction($id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminPanelBundle:Settings')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Settings entity.');
        }

        $editForm = $this->createForm(new SettingsType(), $entity);
        $deleteForm = $this->createDeleteForm($id);

        $data = $editForm->getData()->getKeyValue();

        return $this->render('AdminPanelBundle:Settings:edit.html.php', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'edit_value'  => $data,
        ));
    }

    /**
     * Edits an existing Settings entity.
     *
     */
    public function updateAction(Request $request, $id)
    {
        $em = $this->getDoctrine()->getManager();

        $entity = $em->getRepository('AdminPanelBundle:Settings')->find($id);

        if (!$entity) {
            throw $this->createNotFoundException('Unable to find Settings entity.');
        }

        $deleteForm = $this->createDeleteForm($id);
        $editForm = $this->createForm(new SettingsType(), $entity);
        $editForm->bind($request);

        $data = $editForm->getData()->getKeyValue();

        if ($editForm->isValid()) {
            $em->persist($entity);
            $em->flush();

            return $this->redirect($this->generateUrl('settings_edit', array('id' => $id)));
        }

        return $this->render('AdminPanelBundle:Settings:edit.html.php', array(
            'entity'      => $entity,
            'edit_form'   => $editForm->createView(),
            'delete_form' => $deleteForm->createView(),
            'edit_value'  => $data,
        ));
    }

    /**
     * Deletes a Settings entity.
     *
     */
    public function deleteAction(Request $request, $id)
    {
        $form = $this->createDeleteForm($id);
        $form->bind($request);

        if ($form->isValid()) {
            $em = $this->getDoctrine()->getManager();
            $entity = $em->getRepository('AdminPanelBundle:Settings')->find($id);

            if (!$entity) {
                throw $this->createNotFoundException('Unable to find Settings entity.');
            }

            $em->remove($entity);
            $em->flush();
        }

        return $this->redirect($this->generateUrl('settings'));
    }

    /**
     * Creates a form to delete a Settings entity by id.
     *
     * @param mixed $id The entity id
     *
     * @return Symfony\Component\Form\Form The form
     */
    private function createDeleteForm($id)
    {
        return $this->createFormBuilder(array('id' => $id))
            ->add('id', 'hidden')
            ->getForm()
        ;
    }
}
