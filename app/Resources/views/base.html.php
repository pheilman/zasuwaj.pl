<!DOCTYPE html>
<html>
    <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <meta name="description" content="<?php $view['slots']->output('description', 'Zasuwaj.pl - szkoła rolkowa'); ?>" />
        <title><?php $view['slots']->output('title', 'Zasuwaj.pl - szkoła rolkowa'); ?></title>
        <?php $view['slots']->output('robot', ''); ?>
        <link rel="stylesheet" href="<?php echo $view['assets']->getUrl('css/main.css'); ?>" />
        <link href='http://fonts.googleapis.com/css?family=Oregano' rel='stylesheet' type='text/css'>
    </head>
    <body>
        <?php $view['slots']->output('_content') ?>
    </body>
</html>