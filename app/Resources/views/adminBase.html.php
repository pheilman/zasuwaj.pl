<!DOCTYPE html>
<html>
        <head>
        <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
        <title><?php $view['slots']->output('title', 'Panel administracyjny') ?></title>
        <link rel="stylesheet" href="<?php echo $view['assets']->getUrl('css/adminMain.css'); ?>" />
        <script src="http://code.jquery.com/jquery-1.9.1.min.js"></script>
        <script type="text/javascript" src="<?php echo $view['assets']->getUrl('js/admin.js'); ?>"></script>
    </head>
    <body>
        <?php $view['slots']->output('_content') ?>
    </body>
</html>