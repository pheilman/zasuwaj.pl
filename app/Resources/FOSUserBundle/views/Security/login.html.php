<?php $view->extend('FOSUserBundle::layout.html.php'); ?>

<div class="login-content">
  <form class="login" action="<?php echo $view['router']->generate('fos_user_security_check'); ?>" method="post">
      <h3>Panel administracyjny</h3>
      <div class="line" style="margin-top: 40px;">
        <label for="username">Użytkownik</label>
        <input type="text" id="username" name="_username" value="<?php echo $last_username; ?>" required="required" />
      </div>
      <div class="line">
        <label for="password">Hasło</label>
        <input type="password" id="password" name="_password" required="required" />
      </div>
      <input type="hidden" name="_csrf_token" value="<?php echo $csrf_token; ?>" />
      <input type="hidden" name="_target_path" value="/admin/homepage" />
      <div class="line">
        <input type="submit" id="_submit" name="_submit" value="Zaloguj" />
      </div>
      <div class="line" style="text-align: center;">
        <?php if($error) { ?>
          Zła nazwa użytkownika lub hasło
        <?php } ?>
      </div>
  </form>
</div>

<?php /*
{% if error %}
    <div>{{ error|trans({}, 'FOSUserBundle') }}</div>
{% endif %}

<form action="{{ path("fos_user_security_check") }}" method="post">
    <input type="hidden" name="_csrf_token" value="{{ csrf_token }}" />

    <label for="username">{{ 'security.login.username'|trans({}, 'FOSUserBundle') }}</label>
    <input type="text" id="username" name="_username" value="{{ last_username }}" required="required" />

    <label for="password">{{ 'security.login.password'|trans({}, 'FOSUserBundle') }}</label>
    <input type="password" id="password" name="_password" required="required" />

    <input type="checkbox" id="remember_me" name="_remember_me" value="on" />
    <label for="remember_me">{{ 'security.login.remember_me'|trans({}, 'FOSUserBundle') }}</label>

    <input type="submit" id="_submit" name="_submit" value="{{ 'security.login.submit'|trans({}, 'FOSUserBundle') }}" />
</form>
*/ ?>
